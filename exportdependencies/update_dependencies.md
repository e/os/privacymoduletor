A few orbot dependencies are added directly through the /libs directory, because remotely available version are outdated. We can't embed .aar or .jar into the orbotservice.aar (as any aar), so we have to expose this dependencies in the gitlab repository. 

To do that we use the mn deploy:deploy-file command ; and also the mvn install:install-file command to deploy in local maven repository for development.

1. Update the orbot_service version in
   * exportdependencies/pom.xml file
   * dependencies.gradle
2. For each dependencies in orbot/libs, update or prepare an entry in dependecies.gradle, using groupId=foundation.e ; version orbot_service, and an approprioate artifactId .
3. For each dependencies in orbot/libs, run with appropriate PATH and ARTIFACTID : 

mvn install:install-file \
-DartifactId=[ARTIFACTID] \
-Dpackaging=aar \
-Dfile=[PATH]

4. Later, when isvalidated, deploy to gitlab repository:

mvn deploy:deploy-file \
-DrepositoryId=gitlab-e-maven \
-Durl=https://gitlab.e.foundation/api/v4/projects/1063/packages/maven \
-DartifactId=[ARTIFACTID] \
-Dpackaging=aar \
-Dfile=[PATH]

example, for tor-android-binary
step 2:  
"info.guardianproject:tor-android:$versions.tor_android" -> "foundation.e:tor-android:$versions.orbot_service",

step 3: 

mvn install:install-file \
-DartifactId=tor-android \
-Dpackaging=aar \
-Dfile="../../orbot/libs/tor-android-binary-release.aar

step 4 :

mvn deploy:deploy-file \
-DrepositoryId=gitlab-e-maven \
-Durl=https://gitlab.e.foundation/api/v4/projects/1063/packages/maven \
-DartifactId=tor-android \
-Dpackaging=aar \
-Dfile="../../orbot/libs/tor-android-binary-release.aar
