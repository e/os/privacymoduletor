This document explains how to properly build the module AAR for orbostservice and privacymoduletor from
source.

PrivacyModuleTor includes git repo submodules of OrbotService, the module used by Orbot to run Tor

Be sure that you have all of the git submodules up-to-date:

	git submodule update --init --recursive

You can build the AAR modules :

    ./gradlew :orbotservice:assembleRelease :privacymoduletor:assembleRelease

To deploy localy during development

    ./gradlew --console=verbose publishToMavenLocal

This will put compiled AAR and pom file exposing their dependencies in the local maven
repository (usually in ~/.m2/repository).


To push release on gitlab

    ./gradlew --console=verbose publish

