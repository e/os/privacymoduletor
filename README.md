# Privacy Module Tor

This Module (aar) implements IpScramblerModule functionnalities with Tor, reliying on a fork of OrbotService.

# Build

PrivacyModuleTor includes git repo submodules of OrbotService, the module used by Orbot to run Tor

Be sure that you have all of the git submodules up-to-date:

	git submodule update --init --recursive

You can build the AAR modules :

    ./gradlew :orbotservice:assembleRelease :privacymoduletor:assembleRelease

To deploy localy during development

    ./gradlew --console=verbose publishToMavenLocal

This will put compiled AAR and pom file exposing their dependencies in the local maven
repository (usually in ~/.m2/repository).


To push release on gitlab

    ./gradlew --console=verbose publish

# Update orbotservice to an upper Orbot version

1. Upgrade the code: follow the steps in [orbotservice/README.md](orbotservice/README.md)
2. Prepare the orbotservice the dependencies : follow the steps in [exportdependencies/update_dependencies.md](exportdependencies/update_dependencies.md), first using the mvn install:install-file command
3. build and deploy orbotservice locally: 

    ./gradlew :orbotservice:assembleRelease
   ./gradlew --console=verbose :orbotservice:publishToMavenLocal

4. build and deploy locally  privacymoduletor
   ./gradlew :orbotservice:assembleRelease :privacymoduletor:assembleRelease
   ./gradlew --console=verbose :privacymoduletor:publishToMavenLocal
5. Test this new version with advanced privacy, and merge the orbotservice.
6. deploy the orbotservice dependencies to gitlab registry see [exportdependencies/update_dependencies.md](exportdependencies/update_dependencies.md), using mvn deploy:deploy-file command
7. deploy orbotservice in gitlab registry
   ./gradlew --console=verbose :orbotservice:publish
8. Create privacymoduletor MR. CI will prepare and publish the privacymoduletor.aar .
